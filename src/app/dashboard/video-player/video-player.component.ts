import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, filter, switchMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { Video } from 'src/app/app-types';
import { VideoDataService } from 'src/app/video-data.service';
@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss'],
})
export class VideoPlayerComponent implements OnInit {
  video: Observable<Video>;

  constructor(route: ActivatedRoute, videoSvc: VideoDataService) {
    this.video = route.queryParamMap.pipe(
      map((params) => params.get('videoId') as string),
      filter((id) => !!id),
      switchMap((id) => videoSvc.loadSingleVideo(id))
    );
  }

  ngOnInit(): void {}
}
