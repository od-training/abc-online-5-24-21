import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';

import { Video } from 'src/app/app-types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss'],
})
export class VideoListComponent implements OnInit {
  @Input() selectedId: Observable<string> | undefined;
  @Input() videos: Observable<Video[]> | undefined;

  constructor() {}

  ngOnInit(): void {}
}
